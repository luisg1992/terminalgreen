<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnexosTelefonicosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('anexos_telefonicos', function (Blueprint $table) {
            $table->increments('id_at');
            $table->string('telefono',10);
            $table->string('anexo',10);
            $table->integer('id_local');
            $table->string('codigo_ubic_organÍndice',9)
            $table->string('check_web',1)
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('anexos_telefonicos');
    }
}
